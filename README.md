# InveniOS build manifest

## Getting started

Before doing anything, create and set up an SSH key for your BitBucket account.  
https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html

Once you have your SSH key set up, follow this guide to set up your build environment and download the source:  
https://github.com/nathanchance/Android-Tools/blob/master/Guides/Building_AOSP.txt

Use `repo init -u ssh://git@bitbucket.org/invenionovum/invenios` as the init command.

## Building for the One

Clone this to `.repo/local_manifests`: https://bitbucket.org/invenionovum/android_local_manifest_spearmint/